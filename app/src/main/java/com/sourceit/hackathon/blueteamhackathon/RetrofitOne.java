package com.sourceit.hackathon.blueteamhackathon;


import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

public class RetrofitOne {

        public static final String ENDPOINT = "http://api.fixer.io";
        private static ApiInterface apiInterface;

        static {
            initialize();
        }

        interface ApiInterface{
            @GET("/latest")
            void getBanks(Callback<Resource> callback);
        }

        private static void initialize() {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(ENDPOINT)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            apiInterface = restAdapter.create(ApiInterface.class);
        }

        public static void getCurrency(Callback<Resource> callback) {
            apiInterface.getBanks(callback);
        }

}
