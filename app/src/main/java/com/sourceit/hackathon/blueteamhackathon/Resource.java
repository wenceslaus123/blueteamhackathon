package com.sourceit.hackathon.blueteamhackathon;

import java.util.List;

public class Resource {

    public String base;
    public Rates rates;

    public static class Rates{
        String USD;
        String EUR;
        String RUB;
    }
}
